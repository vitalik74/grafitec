<?php
use app\components\console\Console;
use app\components\handler\Handler;
use app\components\providers\ProgressionProvider;
use app\components\validators\SequenceValidator;

include_once 'vendor/autoload.php';

$console = new Console();
$handler = new Handler(new SequenceValidator(), new ProgressionProvider());
$tests = [
    [// ариф
        'value' => '1,3,5,7,9,11',
        'expect' => true
    ],
    [// геом
        'value' => '2,4,8,16,32,64,128,256,512',
        'expect' => true
    ],
    [
        'value' => '2,4,8,16,10,64,128,256,512',// не верная
        'expect' => false
    ],
    [
        'value' => 'asdfasdfasdfasdf', // не валидные данные, по тз числа через запятую
        'expect' => false
    ]
];

foreach ($tests as $test) {
    $result = $handler->handle($test['value']);

    if ($result['success'] == $test['expect']) {
        $console->output('Test is valid');
    } else {
        $console->output('Test is not valid');
    }
}