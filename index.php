<?php
use app\components\console\Console;
use app\components\handler\Handler;
use app\components\providers\ProgressionProvider;
use app\components\validators\SequenceValidator;

include_once 'vendor/autoload.php';

$console = new Console();
$handler = new Handler(new SequenceValidator(), new ProgressionProvider());

$console->output('Please, input sequence: ', '');
$result = $handler->handle($console->input());
$console->output($result['success'] ? $result['msg'] : $result['errorMsg']);
