<?php

namespace app\components\console;

/**
 * Class Console
 *
 * @package app\components\console
 */
class Console
{
    /**
     * @param bool $raw
     *
     * @return string
     */
    public function input($raw = false)
    {
        return $raw ? fgets(\STDIN) : rtrim(fgets(\STDIN), PHP_EOL);
    }

    /**
     * @param $string
     * @param string $eol
     *
     * @return int
     */
    public function output($string, $eol = PHP_EOL)
    {
        return fwrite(\STDOUT, $string . $eol);
    }
}
