<?php

namespace app\components\validators;

/**
 * Class SequenceValidator
 *
 * @package app\components\validators
 */
class SequenceValidator implements ValidatorInterface
{
    /**
     * @param $attributes
     *
     * @return bool
     */
    public function validate($attributes)
    {
        $sequence = $attributes['sequence'];

        if (preg_match('/^(?:\d\,?)+\d$/is', $sequence)) {
            return true;
        }

        return false;
    }
}
