<?php
namespace app\components\validators;

/**
 * Interface ValidatorInterface
 *
 * @package app\components\validators
 */
interface ValidatorInterface {
    /**
     * @param $attributes
     *
     * @return bool
     */
    public function validate($attributes);
}
