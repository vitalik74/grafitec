<?php

namespace app\components\handler;

use app\components\providers\progressions\Arithmetical;
use app\components\providers\progressions\Geometry;
use app\components\providers\ProgressionProvider;
use app\components\validators\ValidatorInterface;

/**
 * Class Handler
 *
 * @package app\components\handler
 */
class Handler
{
    /**
     * @var ValidatorInterface
     */
    private $_validator;
    /**
     * @var
     */
    private $_progressionProvider;


    /**
     * Handler constructor.
     *
     * @param ValidatorInterface $validator
     * @param ProgressionProvider $progressionProvider
     */
    public function __construct(ValidatorInterface $validator, ProgressionProvider $progressionProvider)
    {
        $this->_validator = $validator;
        $this->_progressionProvider = $progressionProvider;
        // добавляем прогрессии, какие нам надо
        $this->_progressionProvider->setProgressions([
            Geometry::class, Arithmetical::class
        ]);
    }

    /**
     * @param $sequence
     *
     * @return array
     */
    public function handle($sequence)
    {
        $validate = $this->_validator->validate(['sequence' => $sequence]);

        if (!empty($validate)) {
            $data = explode(',', $sequence);

            foreach ($this->_progressionProvider->getProgressionsInstance() as $progression) {
                if ($progression->check($data)) {
                    return [
                        'success' => true,
                        'msg' => 'It is ' . $progression->name() . ' progression.'
                    ];
                }
            }

            return [
                'success' => false,
                'errorMsg' => 'It is not progression.'
            ];
        }

        return [
            'success' => false,
            'errorMsg' => 'Validation error: please, input sequence like: 1,3,5,7,9,11'
        ];
    }
}
