<?php

namespace app\components\providers\progressions;

/**
 * Геометрическая прогрессия
 *
 * @package app\components\providers\progressions
 */
class Geometry implements ProgressionInterface
{
    /**
     * Проверка прогрессии
     *
     * @param array $data
     *
     * @return bool
     */
    public function check(array $data)
    {
        if (count($data) < 2) {// минимум два число должно быть
            return false;
        }

        if ($data[0] == 0) { // b1 не может равен нулю
            return false;
        }

        // вычислим q
        $b1 = $data[0];
        $q = $data[1] / $b1;

        if ($q == 0) {// не может быть равна нулю
            return false;
        }

        foreach ($data as $k => $value) {
            if ($k > 0) {// со второго элемента
                $expect = $b1 * pow($q, $k);

                if ($expect != $value) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Вывод названия прогрессии
     *
     * @return string
     */
    public function name()
    {
        return 'Geometry';
    }
}
