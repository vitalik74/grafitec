<?php

namespace app\components\providers\progressions;

/**
 * Interface ProgressionInterface
 *
 * @package app\components\providers\progressions
 */
interface ProgressionInterface {
    /**
     * Проверка прогрессии
     *
     * @param array $data
     *
     * @return mixed
     *
     */
    public function check(array $data);

    /**
     * Вывод названия прогрессии
     *
     * @return string
     */
    public function name();
}
