<?php

namespace app\components\providers\progressions;

/**
 * Арифметическая прогрессия
 *
 * @package app\components\providers\progressions
 */
class Arithmetical implements ProgressionInterface
{
    /**
     * Проверка прогрессии
     *
     * @param array $data
     *
     * @return mixed
     */
    public function check(array $data)
    {
        $a1 = $data[0];
        $d = $data[1] - $a1;// вычисляем d

        foreach ($data as $k => $value) {
            if ($k > 0) {// со второго элемента
                $expect = $a1 + ($k * $d);

                if ($expect != $value) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Вывод названия прогрессии
     *
     * @return string
     */
    public function name()
    {
        return 'Arithmetical';
    }
}
