<?php

namespace app\components\providers;

use app\components\providers\progressions\ProgressionInterface;

/**
 * Class ProgressionProvider
 *
 * @package app\components\providers
 */
class ProgressionProvider
{
    /**
     * @var array
     */
    private $_progressions;


    /**
     * @return ProgressionInterface[]
     */
    public function getProgressionsInstance()
    {
        $result = [];

        foreach ($this->_progressions as $progression) {
            $result[] = new $progression();
        }

        return $result;
    }

    /**
     * @param array $progressions
     */
    public function setProgressions(array $progressions)
    {
        foreach ($progressions as $progression) {
            if ((new $progression()) instanceof ProgressionInterface) {
                $this->_progressions[] = $progression;
            }
        }
    }
}
